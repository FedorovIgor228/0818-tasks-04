
#include <stdio.h>
#include <string.h>
#include <locale.h>
#include <stdlib.h>
#include <time.h>

#define INPUT "C:\\input.txt"
#define ANSWER "C:\\answer.txt"
#define MAXLENGTH 255 
int main() 
{
	FILE *in, *out;
	unsigned int length,rand_pos,sum_word, pos_words[MAXLENGTH];
	char str[MAXLENGTH],nstr[MAXLENGTH];
	srand(time(0));
	in = fopen(INPUT, "rt");
	out = fopen(ANSWER, "w");
	if (in) 
	{
		while (fgets(str, MAXLENGTH, in)) 
		{
			for (int i = 0; i < MAXLENGTH; i++)
				pos_words[i] = 0;
			sum_word = 0;
			length = strlen(str);
			if (length - 1 == '\n')
				str[length - 1] = 0;
			for (int i = 0; i < length; i++) {
				if (str[i] != ' ' && (i == 0 || str[i - 1] == ' ')) 
				{
					pos_words[sum_word] = i;
					sum_word++;
				}
			}
			int temp;
			for (int i = 0; i < sum_word; i++)
			{
				rand_pos = rand() % sum_word;
				temp = pos_words[rand_pos];
				pos_words[rand_pos] = pos_words[i];
				pos_words[i] = temp;
			}
			int j, k = 0;
			for (int i = 0; i < sum_word; i++)
			{
				j = pos_words[i];
				while (str[j] != ' ' && str[j] != 0) 
				{
					nstr[k] = str[j];
					j++;
					k++;
				}
				nstr[k] = ' ';
				k++;
			}
			nstr[k - 1] = '\n';
			nstr[k] = 0;
			fputs(nstr, out);
		}
		printf("All done. Check %s\n", ANSWER);
		fclose(in);
		fclose(out);
	}
	else {
		printf("Error.Check Input file");
	}
	return 0;
}
#include <stdio.h>
#include <locale.h>

int main() 
{
	int	count, max_count = 0;
	long long int num, answer;
	for (int i = 2; i <= 1000000; i++) 
	{
		num = i;
		count = 1;
		while (num != 1) 
		{
			if (num % 2 == 0)
				num /= 2;
			else
			{
			 num= 3 * num + 1;
			}

			count++;
		}
		if (count > max_count) 
		{
			max_count = count;
			answer = i;
		}
	}
	printf("Answer: %d\n", answer);
	return 0;
}